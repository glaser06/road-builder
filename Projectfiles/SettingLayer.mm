//
//  SettingLayer.m
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 8/15/13.
//
//

#import "SettingLayer.h"
#import "MenuLayer.h"

@implementation SettingLayer
CCLabelTTF *tutorialLb;
-(id)init
{
    if ((self = [super init]))
    {
        CCSprite *background  = [CCSprite spriteWithFile:@"construction_app_bg.png"];
        background.position=CGPointZero;
        background.anchorPoint=CGPointZero;
        [self addChild:background];
        
        NSString *tutorialText = @"Turn Tutorial On";

        bool tutorial = [[[NSUserDefaults standardUserDefaults]objectForKey:@"tutorial"]boolValue];
        if(tutorial)
        {
            tutorialText = @"Turn Tutorial Off";
        }
        else
        {
            tutorialText = @"Turn Tutorial On";
        }
        CCMenuItemImage * tutorialBt = [CCMenuItemImage itemWithNormalImage:@"btn.png" selectedImage:@"btn.png" target:self selector:@selector(tutorialChange)];
        tutorialBt.position = ccp([[CCDirector sharedDirector]screenCenter].x,220);
        CCMenuItemImage * resetBt = [CCMenuItemImage itemWithNormalImage:@"btn.png" selectedImage:@"btn.png" target:self selector:@selector(resetChange)];
        resetBt.position = ccp([[CCDirector sharedDirector]screenCenter].x,170);
        CCMenuItemImage * menuBt = [CCMenuItemImage itemWithNormalImage:@"btn.png"
                                                          selectedImage: @"btn.png"
                                                                 target:self
                                                               selector:@selector(menuChange)];
        menuBt.position = ccp([[CCDirector sharedDirector]screenCenter].x,110);
        
        CCMenu * myMenu = [CCMenu menuWithItems:tutorialBt,resetBt, menuBt, nil];
//        [myMenu alignItemsVertically];
        myMenu.position = CGPointZero;//ccp([[CCDirector sharedDirector]screenCenter].x,[[CCDirector sharedDirector]screenCenter].y-40);
        [self addChild:myMenu];
        CCLabelTTF *menu = [CCLabelTTF labelWithString:@"Main Menu" fontName:@"Frutiger LT Std" fontSize:20];
        menu.position = ccp([[CCDirector sharedDirector]screenCenter].x,106);
        [self addChild:menu];
        tutorialLb = [CCLabelTTF labelWithString:tutorialText fontName:@"Frutiger LT Std" fontSize:16];
        tutorialLb.position = ccp([[CCDirector sharedDirector]screenCenter].x,218);
        [self addChild:tutorialLb];
        CCLabelTTF *resetLb = [CCLabelTTF labelWithString:@"Reset Stats" fontName:@"Frutiger LT Std" fontSize:20];
        resetLb.position = ccp([[CCDirector sharedDirector]screenCenter].x,165);
        [self addChild:resetLb];
    }
    return self;
}
-(void)menuChange
{
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[MenuLayer scene]]];
}
-(void)tutorialChange
{
    bool tutorial = [[[NSUserDefaults standardUserDefaults]objectForKey:@"tutorial"]boolValue];
    [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:!tutorial] forKey:@"tutorial"];
    if(tutorial)
    {
        [tutorialLb setString: @"Turn Tutorial On"];
    }
    else
    {
        [tutorialLb setString: @"Turn Tutorial Off"];
    }
}
-(void)resetChange
{
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"roads"];
    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"maps"];
}
+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	SettingLayer *layer = [SettingLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

@end
