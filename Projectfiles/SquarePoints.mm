//
//  SquarePoints.m
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SquarePoints.h"

@implementation SquarePoints
@synthesize selected;
@synthesize built;
+(id) square
{
	id square = [[self alloc] initWithSquareImage];
#ifndef KK_ARC_ENABLED
	[ship autorelease];
#endif // KK_ARC_ENABLED
	return square;
}

-(id) initWithSquareImage
{
	// Loading the Ship's sprite using a sprite frame name (eg the filename)
	if ((self = [super initWithFile:@"city_icon.png"]))
	{
		selected = false;
        built = false;
	}
	return self;
}

-(void) setTrue
{
    prev = true;
}
-(void) setFalse
{
    prev=false;
}
-(bool) getState
{
    return prev;
}
-(void) setPoint:(int) k
{
    previous = k;
}
-(int) returnPoint
{
    return previous;
}
-(void) setStart
{
    start=true;
}
-(bool) returnStart
{
    return start;
}
-(void)setMountain
{
    mountain=true;
}
-(bool)returnMountain
{
    return mountain;
}
-(void)setPair:(CGPoint) i second:(CGPoint) j
{
    pair = i;
    pairA = j;
}
-(CGPoint)returnPair
{
    return pair;
}
-(CGPoint)returnPairA
{
    return pairA;
}
-(void) setTeleporter;
{
    teleporter = true;
}
-(bool) returnTeleporter;
{
    return teleporter;
}
-(void) setPair2:(CGPoint) i second:(CGPoint) j
{
    pair2 = i;
    pair2A = j;
}
-(CGPoint) returnPair2;
{
    return pair2;
}
-(CGPoint) returnPair2A;
{
    return pair2A;
}
-(void)setOtherMountain
{
    bridges=true;
}
-(bool)returnOtherMountain
{
    return bridges;
}
-(void)setPair3:(CGPoint)i
{
    pair3=i;
}
-(CGPoint)returnPair3
{
    return pair3;
}
@end
