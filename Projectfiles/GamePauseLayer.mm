//
//  GameOverLayer.m
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "GamePauseLayer.h"
#import "MenuLayer.h"
#import "GameLayer.h"
#import "SimpleAudioEngine.h"
#import "CCDirector+PopTransition.h"
@implementation GamePauseLayer
-(id)init
{
    if ((self = [super init]))
    {
//        NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"highScore"];
//        int hs = [currentHighScore intValue];
//        CCLabelTTF *label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", hs]
//                                               fontName:@"Frutiger LT Std"
//                                               fontSize:20];
//        label.position = CGPointMake(240, 230);
//        glLineWidth(2.0f);
//        label.color = ccRED;
//        [self addChild:label];
        CCSprite *background  = [CCSprite spriteWithFile:@"construction_app_bg.png"];
        background.position=CGPointZero;
        background.anchorPoint=CGPointZero;
        [self addChild:background];
        CCSprite *pausedImage = [CCSprite spriteWithFile:@"paused.png"];
        pausedImage.position = ccp([[CCDirector sharedDirector]screenCenter].x,250);
        [self addChild:pausedImage];
        CCMenuItemImage * resumeBt = [CCMenuItemImage itemWithNormalImage:@"btn.png"
                                                             selectedImage: @"btn.png"
                                                                    target:self
                                                                  selector:@selector(resumeChange)];
        resumeBt.position = ccp(140,180);
        
        CCMenuItemImage * lvlselBt = [CCMenuItemImage itemWithNormalImage:@"btn.png"
                                                             selectedImage: @"btn.png"
                                                                    target:self
                                                                  selector:@selector(levelSelectChange)];
        lvlselBt.position = ccp(140,110);
        CCMenuItemImage * restartBt = [CCMenuItemImage itemWithNormalImage:@"btn.png"
                                                             selectedImage: @"btn.png"
                                                                    target:self
                                                                  selector:@selector(restartChange)];
        restartBt.position = ccp(340,180);
        
        CCMenuItemImage * menuBt = [CCMenuItemImage itemWithNormalImage:@"btn.png"
                                                             selectedImage: @"btn.png"
                                                                    target:self
                                                                  selector:@selector(menuChange)];
        menuBt.position = ccp(340,110);
        
        //NSLog(@"%i",hs);
        CCMenu * myMenu = [CCMenu menuWithItems:restartBt,resumeBt,menuBt, nil];
        [myMenu alignItemsVertically];
        myMenu.position = ccp([[CCDirector sharedDirector]screenCenter].x,[[CCDirector sharedDirector]screenCenter].y-40);
        //[myMenu addChild:menuItem4];
        
        [self addChild:myMenu];
        CCLabelTTF *resume = [CCLabelTTF labelWithString:@"Resume" fontName:@"Frutiger LT Std" fontSize:20];
        //resume setFontName:<#(NSString *)#>
        resume.position = ccpAdd(resumeBt.position,ccp([[CCDirector sharedDirector]screenCenter].x, [[CCDirector sharedDirector]screenCenter].y-40));
        [self addChild:resume];
        CCLabelTTF *restart = [CCLabelTTF labelWithString:@"Restart" fontName:@"Frutiger LT Std" fontSize:20];
        restart.position = ccpAdd(restartBt.position,ccp([[CCDirector sharedDirector]screenCenter].x, [[CCDirector sharedDirector]screenCenter].y-40));
        [self addChild:restart];
        CCLabelTTF *menu = [CCLabelTTF labelWithString:@"Main Menu" fontName:@"Frutiger LT Std" fontSize:20];
        menu.position = ccpAdd(menuBt.position,ccp([[CCDirector sharedDirector]screenCenter].x, [[CCDirector sharedDirector]screenCenter].y-40));
        [self addChild:menu];
        CCLabelTTF *lvlSel = [CCLabelTTF labelWithString:@"Level Select" fontName:@"Frutiger LT Std" fontSize:20];
        lvlSel.position = ccpAdd(lvlselBt.position,ccp([[CCDirector sharedDirector]screenCenter].x, [[CCDirector sharedDirector]screenCenter].y-40));
//        [self addChild:lvlSel];
        
        
    }
    return self;
}
+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GamePauseLayer *layer = [GamePauseLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(void)menuChange
{
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[MenuLayer scene]]];
}
-(void)resumeChange
{
    [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
    [[CCDirector sharedDirector] popSceneWithTransition:[CCTransitionFade class] duration:.6f];
}
-(void) levelSelectChange
{
//    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"levels"];
//    int hs = [currentHighScore intValue];
}
-(void) restartChange
{
//    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"levels"];
//    int hs = [currentHighScore intValue];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:0.6f scene:[GameLayer showLevel:1 score:0 concrete:0]]];
}
@end
