//
//  MenuLayer.m
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuLayer.h"
#import "GameLayer.h"
#import "SettingLayer.h"
#import "SimpleAudioEngine.h"
//#import "GameOverLayer.h"
//#import "GameLevelLayer.h"
@implementation MenuLayer
-(id) init
{
        if ((self = [super init]))
        {
            CCSprite *background = [CCSprite spriteWithFile:@"play_screen_bg.png"];
            background.position = CGPointZero;
            background.anchorPoint = CGPointZero;
            [self addChild:background];
//            [[NSUserDefaults standardUserDefaults] setObject:@true forKey:@"tutorial"];
            
            NSNumber *highScore = [NSNumber numberWithInteger:0];
            [[NSUserDefaults standardUserDefaults] setObject:highScore forKey:@"levels"];
//            [[NSUserDefaults standardUserDefaults] setObject:0 forKey:@"roads"];
            //CCDirector* director = [CCDirector sharedDirector];
            glClearColor(0.1f, 0.0f, 0.2f, 1.0f);
           /* CCLabelTTF* label = [CCLabelTTF labelWithString:@"ads;lfknasd;flknads;fln"
                                                   fontName:@"Arial"
                                                   fontSize:30];
            label.position = director.screenCenter;
            label.color = ccGREEN;
            
            [self addChild:label];*/
            
            
           /* 
            CCMenu *menu = [CCMenu menuWithItems: nil];
            CCMenuItemLabel *myLabel=[CCMenuItemLabel itemWithLabel:label target:self selector:@selector(random)];
            [menu addChild:myLabel];
            [menu alignItemsHorizontally];
            [self addChild:menu];*/
            CCMenuItemImage * playButton = [CCMenuItemImage itemWithNormalImage:@"play.png"
                                                                 selectedImage: @"play.png"
                                                                        target:self
                                                                      selector:@selector(GameLevelLayerChange)];
            int center = [[CCDirector sharedDirector]screenCenter].x;
            playButton.position = ccp(center+10,120);
            CCMenu *playMenu = [CCMenu menuWithItems:playButton, nil];
            playMenu.position=CGPointZero;
            [self addChild:playMenu];
            CCMenuItemImage * levelSelect = [CCMenuItemImage itemWithNormalImage:@"btn.png" selectedImage:@"btn.png" target:self selector:@selector(settingChange)];
            //levelSelect.position = ccp(80,30);
            
            CCMenuItemImage * aboutBT = [CCMenuItemImage itemWithNormalImage:@"btn.png" selectedImage:@"btn.png" target:self selector:@selector(gameAboutChange)];
            //aboutBT.position = ccp(240,30);
            
            CCMenuItemImage * moreGameBT = [CCMenuItemImage itemWithNormalImage:@"btn.png" selectedImage:@"btn.png" target:self selector:@selector(gameMoreGamesChange)];
            //moreGameBT.position = ccp(400,30);
            
            
            CCMenu * myMenu = [CCMenu menuWithItems:levelSelect,aboutBT,moreGameBT, nil];
            [myMenu alignItemsHorizontally];
            myMenu.position=ccp([[CCDirector sharedDirector]screenCenter].x,40);
            [self addChild:myMenu];
            
            CCLabelTTF *lvlSelTxt = [CCLabelTTF labelWithString:@"Settings" fontName:@"Frutiger LT Std" fontSize:23];
            lvlSelTxt.position=ccpAdd(levelSelect.position,ccp([[CCDirector sharedDirector]screenCenter].x, [[CCDirector sharedDirector]screenCenter].y-125));
            [self addChild:lvlSelTxt];
            CCLabelTTF *aboutTxt = [CCLabelTTF labelWithString:@"About" fontName:@"Frutiger LT Std" fontSize:23];
            aboutTxt.position = ccpAdd(aboutBT.position,ccp([[CCDirector sharedDirector]screenCenter].x, [[CCDirector sharedDirector]screenCenter].y-125));
            [self addChild:aboutTxt];
            CCLabelTTF *moreGameTxt = [CCLabelTTF labelWithString:@"More Games" fontName:@"Frutiger LT Std" fontSize:20];
            moreGameTxt.position = ccpAdd(moreGameBT.position,ccp([[CCDirector sharedDirector]screenCenter].x, [[CCDirector sharedDirector]screenCenter].y-125));
            [self addChild:moreGameTxt];
            
            
            
        }
    return self;
}
+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MenuLayer *layer = [MenuLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene 
	return scene;
}
-(void) random
{
    //NSLog(@"printing stuff asdlfk ja;lsdknf l;asdknf;laksdnf;alsdknf");
//    [[SimpleAudioEngine sharedEngine] playEffect:@"explo2.wav"];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[GameLayer showLevel:1 score:0 concrete:0]]];
}
-(void) GameLevelLayerChange
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"explo2.wav"];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[GameLayer showLevel:1 score:0 concrete:0]]];
}
-(void) gameMoreGamesChange
{
    [MGWU displayCrossPromo];
}
-(void)gameAboutChange
{
    [MGWU displayAboutPage];
}
-(void)settingChange
{
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.5f scene:[SettingLayer scene]]];
}
@end
