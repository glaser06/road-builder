//
//  GameOverLayer.m
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameOverLayer.h"
#import "MenuLayer.h"
#import "GameLayer.h"
@implementation GameOverLayer
-(id)init
{
    if ((self = [super init]))
    {
        CCSprite *background = [CCSprite spriteWithFile:@"construction_app_bg.png"];
        background.anchorPoint=CGPointZero;
        background.position=CGPointZero;
        [self addChild: background];

        NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"highScore"];
        int hs = [currentHighScore intValue];
        
        CCLabelTTF *label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", hs]
                                   fontName:@"Frutiger LT Std"
                                   fontSize:20];
        label.position = CGPointMake(240, 300);
        glLineWidth(2.0f);
        label.color = ccRED;
        [self addChild:label];
        CCLabelTTF *label1 = [CCLabelTTF labelWithString:@"You Passed!"
                                               fontName:@"Frutiger LT Std"
                                               fontSize:20];
        label1.position = CGPointMake(240, 260);
        glLineWidth(2.0f);
        label1.color = ccRED;
        [self addChild:label1];
        CCMenuItemImage * menuItem1 = [CCMenuItemImage itemWithNormalImage:@"btn.png"
                                                         selectedImage: @"btn.png"
                                                                target:self
                                                              selector:@selector(menuChange)];
        menuItem1.position = ccp(240,80);
        CCMenuItemImage * menuItem3 = [CCMenuItemImage itemWithNormalImage:@"btn.png"
                                                             selectedImage: @"btn.png"
                                                                    target:self
                                                                  selector:@selector(retryChange)];
        menuItem3.position = ccp(240,160);
        CCMenuItemImage * menuItem2 = [CCMenuItemImage itemWithNormalImage:@"btn.png"
                                                             selectedImage: @"btn.png"
                                                                    target:self
                                                                  selector:@selector(nextLvlChange)];
        menuItem2.position = ccp(240,220);

        //NSLog(@"%i",hs);
        CCMenu * myMenu = [CCMenu menuWithItems:menuItem2,menuItem3,menuItem1, nil];
        [myMenu alignItemsVertically];
        //myMenu.position;
        [self addChild:myMenu];
        CCLabelTTF *menuTxt = [CCLabelTTF labelWithString:@"Main Menu" fontName:@"Frutiger LT Std" fontSize:20];
        menuTxt.position = ccp([[CCDirector sharedDirector]screenCenter].x,90);
        CCLabelTTF *retryText = [CCLabelTTF labelWithString:@"Retry" fontName:@"Frutiger LT Std" fontSize:25];
        retryText.position=ccp([[CCDirector sharedDirector]screenCenter].x,155);
        CCLabelTTF *nextLvlTxt = [CCLabelTTF labelWithString:@"Keep Building" fontName:@"Frutiger Lt Std" fontSize:20];
        nextLvlTxt.position=ccp([[CCDirector sharedDirector]screenCenter].x,220);
        [self addChild:menuTxt];
        [self addChild:nextLvlTxt];
        [self addChild:retryText];
    }
    return self;
}
+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameOverLayer *layer = [GameOverLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(void)menuChange
{
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[MenuLayer scene]]];
}
-(void)nextLvlChange
{
    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"levels"];
    int hs = [currentHighScore intValue];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer showLevel:hs+1 score:0 concrete:0]]];
}
-(void) retryChange
{
    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"levels"];
    int hs = [currentHighScore intValue];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer showLevel:hs score:0 concrete:0]]];
}
@end
