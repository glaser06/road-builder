//
//  GameOverLayer.m
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameLossLayer.h"
#import "MenuLayer.h"
#import "GameLayer.h"

//int roads = 0;
@implementation GameLossLayer
-(id)init
{
    if ((self = [super init]))
    {
        NSString *fontName = @"Frutiger LT Std";
        CCSprite *background = [CCSprite spriteWithFile:@"construction_app_bg.png"];
        background.anchorPoint=CGPointZero;
        background.position=CGPointZero;
        [self addChild: background z:-1];
        
        NSString *lossText = @"Game Over";
        
        CCLabelTTF *label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", [GameLayer getRoads] ]
                                               fontName:fontName
                                               fontSize:20];
        label.position = CGPointMake(284, 290);
        glLineWidth(2.0f);
        label.color = ccGRAY;
//        [self addChild:label z:20];
        
        CCSprite *roadSprite = [CCSprite spriteWithFile:@"concreteroad_vert.png"];
        roadSprite.position = ccp(360,230);
//        [self addChild:roadSprite z:20];
        int best = [[[NSUserDefaults standardUserDefaults] objectForKey:@"roads"]intValue];
        int roads = [GameLayer getRoads];
        int bestMaps = [[[NSUserDefaults standardUserDefaults] objectForKey:@"maps"]intValue];
        int levels = [GameLayer getLevel]-1;
        if(roads>best || levels>bestMaps)
            lossText = @"You Beat Your Highscore!!";
        [self highscore];
        best = [[[NSUserDefaults standardUserDefaults] objectForKey:@"roads"]intValue];
        bestMaps = [[[NSUserDefaults standardUserDefaults] objectForKey:@"maps"]intValue];
        
        CCLabelTTF *label1 = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"You built %i ",[GameLayer getRoads]]
                                                fontName:fontName
                                                fontSize:20];
        label1.position = CGPointMake(284, 230);
        glLineWidth(2.0f);
        label1.color = ccGRAY;
//        [self addChild:label1];
        CCLabelTTF *gameOver = [CCLabelTTF labelWithString:lossText fontName:fontName fontSize:30];
        gameOver.position = ccp([[CCDirector sharedDirector]screenSize].width/2,260);
        gameOver.color = ccRED;
        [self addChild:gameOver];
        
        CCLabelTTF *roadNum = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Roads Built: %i",roads] fontName:fontName fontSize:20];
        roadNum.position = ccp(120,200);
        roadNum.color = ccBLACK;
        [self addChild:roadNum];
        
        CCLabelTTF *concreteNum = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Concrete Used: %i",[GameLayer getConcrete]] fontName:fontName fontSize:20];
        concreteNum.position = ccp(120,155);
        concreteNum.color = ccBLACK;
        [self addChild:concreteNum];
        
        CCLabelTTF *levelLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Maps Completed: %i",[GameLayer getLevel]-1] fontName:fontName fontSize:20];
        levelLabel.position = ccp(120,110);
        levelLabel.color = ccBLACK;
        [self addChild:levelLabel];
        int width = [[CCDirector sharedDirector]screenSize].width;
        CCLabelTTF *personalBest = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Personal Best"] fontName:fontName fontSize:25];
        personalBest.position = ccp(width-128,220);
        personalBest.color = ccBLUE;
        [self addChild:personalBest];
        
        CCLabelTTF *bestRoad = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Roads Built: %i",best] fontName:fontName fontSize:20];
        bestRoad.position = ccp(width-128,185);
        bestRoad.color = ccBLACK;
        [self addChild:bestRoad];
        
        CCLabelTTF *bestMap = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Maps Completed: %i",bestMaps] fontName:fontName fontSize:20];
        bestMap.position = ccp(width-128,140);
        bestMap.color = ccBLACK;
        [self addChild:bestMap];
        
        [MGWU submitHighScore:roads byPlayer:@"Player" forLeaderboard:@"RoadGloryRoads"];
        
        CCMenuItemImage * menuItem1 = [CCMenuItemImage itemWithNormalImage:@"btn.png"
                                                             selectedImage: @"btn.png"
                                                                    target:self
                                                                  selector:@selector(random2)];
        menuItem1.position = ccp([[CCDirector sharedDirector]screenSize].width/2,45);
//        CCMenuItemImage * menuItem3 = [CCMenuItemImage itemWithNormalImage:@"mythirdbutton.png"
//                                                             selectedImage: @"mythirdbutton_selected.png"
//                                                                    target:self
//                                                                  selector:@selector(random3)];
        CCMenuItemImage * menuItem2 = [CCMenuItemImage itemWithNormalImage:@"smallbtn.png"
                                                             selectedImage: @"smallbtn.png"
                                                                    target:self
                                                                  selector:@selector(random)];
        menuItem2.position = ccp(50,300);
        CCMenu * myMenu = [CCMenu menuWithItems:menuItem1,menuItem2, nil];
        myMenu.position = CGPointZero;
        [self addChild:myMenu];
        CCLabelTTF *btntext1 = [CCLabelTTF labelWithString:@"Restart" fontName:fontName fontSize:23];
        CCLabelTTF *btntext2 = [CCLabelTTF labelWithString:@"Main Menu" fontName:fontName fontSize:10];
        btntext1.position=ccp([[CCDirector sharedDirector]screenSize].width/2,40);
        btntext2.position=ccp(50,298);
        [self addChild:btntext1];
        [self addChild:btntext2];
    }
    return self;
}
+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameLossLayer *layer = [GameLossLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(void)highscore
{
    int road = [GameLayer getRoads];
    int best = [[[NSUserDefaults standardUserDefaults] objectForKey:@"roads"]intValue];
    int map = [GameLayer getLevel]-1;
    int bestMap = [[[NSUserDefaults standardUserDefaults] objectForKey:@"maps"]intValue];
    if(road>best)
    {
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:road] forKey:@"roads"];
    }
    if(map>bestMap)
    {
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:map] forKey:@"maps"];
    }
}
+(id) lossTransition:(int)score
{
//    roads = score;
    CCScene *scene = [GameLossLayer scene];
    return scene;
}
-(void)random
{
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[MenuLayer scene]]];
}
-(void)random2
{
//    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"levels"];
//    int hs = [currentHighScore intValue];
//    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"roads"];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer showLevel:1 score:0 concrete:0]]];
}
-(void) random3
{
//    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"levels"];
//    int hs = [currentHighScore intValue];
//    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"roads"];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer showLevel:1 score:0 concrete:0]]];
}
@end
