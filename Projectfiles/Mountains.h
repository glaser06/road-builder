//
//  Mountains.h
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "Box2D.h"
#import "GameLayer.h"

@interface Mountains : CCSprite
{
    
}
+(id)mountain;
-(id)initWithMountainImage;

@end