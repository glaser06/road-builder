//
//  OtherMountain.m
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OtherMountain.h"

@implementation OtherMountain
+(id) OtherMountain
{
	id OtherMountain = [[self alloc] initWithOtherMountainImage];
    
	return OtherMountain;
}

-(id) initWithOtherMountainImage
{
	// Loading the Ship's sprite using a sprite frame name (eg the filename)
	if ((self = [super initWithFile:@"smalltown_icon.png"]))
	{
		
	}
	return self;
}
@end

