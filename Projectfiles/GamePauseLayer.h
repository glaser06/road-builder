//
//  GameOverLayer.h
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 6/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "cocos2d.h"
#import "Box2D.h"

@interface GamePauseLayer : CCLayer
+(id) scene;

@end
