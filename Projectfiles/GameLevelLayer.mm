////
////  GameLevelLayer.m
////  squirrellls
////
////  Created by Zaizen Kaegyoshi on 7/3/12.
////  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
////
//
//#import "GameLevelLayer.h"
//#import "MenuLayer.h"
//#import "GameLayer.h"
//#import "GameOverLayer.h"
//@implementation GameLevelLayer
//-(id)init
//{
//    if ((self = [super init]))
//    {
//        
//        CCSprite *background = [CCSprite spriteWithFile:@"construction_app_levelscreen_bg.png"];
//        background.position = CGPointZero;
//        background.anchorPoint = CGPointZero;
//        [self addChild:background];
//        
//        //back to main menu button
//        CCMenuItemImage *backBt = [CCMenuItemImage itemWithNormalImage:@"level_btn.png" selectedImage:@"level_btn.png" target:self selector:@selector(menuChange)];
//        backBt.position = ccp(40,280);
//        CCMenu *btMenu = [CCMenu menuWithItems:backBt, nil];
//        btMenu.position=CGPointZero;
//        [self addChild:btMenu];
//        
//        //tutorial button
////        CCMenuItemImage *tutorial = [CCMenuItemImage itemWithNormalImage:@"btn.png" selectedImage:@"btn.png"];
////        tutorial.position = ccp(240,215);
////        [btMenu addChild:tutorial];
////        CCLabelTTF *tutorialTxt = [CCLabelTTF labelWithString:@"Tutorial" fontName:@"Frutiger LTStd" fontSize:25];
////        tutorialTxt.position = ccp(240,220);
////        [self addChild:tutorialTxt];
//        
//                
//        
//        
//        
//        //level menu and text
//        int lvl=0;
//        CCMenu *myMenu= [CCMenu menuWithItems: nil];
//        for(int col=1;col<=4;col++)
//        {
//            for(int row=1;row<=5;row++)
//            {
//                CCMenuItemImage *menuItem = [CCMenuItemImage itemWithNormalImage:@"level_btn.png" selectedImage:@"level_btn.png" target:self selector:@selector(levelChange:)];
//                menuItem.tag = lvl;
//                //menuItem.position = ccp(80*row,-100+45*(7-col));
//                [myMenu addChild:menuItem];
//                lvl ++;
//            }
//        }
//        
//        [myMenu alignItemsInColumns:[NSNumber numberWithInt:5],[NSNumber numberWithInt:5],[NSNumber numberWithInt:5],[NSNumber numberWithInt:5], nil];
//        myMenu.position= ccp([[CCDirector sharedDirector] screenCenter].x,135);
//        [self addChild:myMenu];
//        lvl = 1;
//        for(int col=1;col<=4;col++)
//        {
//            for(int row=1;row<=5;row++)
//            {
//                
//                
//                CCLabelTTF *lvlNum = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i",lvl] fontName:@"Frutiger LT Std" fontSize:25];
//                CCMenuItem *temp = (CCMenuItem*)[myMenu getChildByTag:lvl-1];
//                //CGPoint x= temp.position;
//                lvlNum.position=ccpAdd(temp.position,ccp([[CCDirector sharedDirector]screenCenter].x, [[CCDirector sharedDirector]screenCenter].y-30));
//                //lvlNum.position=ccp(temp.position.x+[[CCDirector sharedDirector]screenCenter].x,temp.position.y+[[CCDirector sharedDirector]screenCenter].y-30);
//                [self addChild:lvlNum z:2];
//                lvl++;
//                
//                
//            }
//        }
//
//    }
//    return self;
//}
//+(id) scene
//{
//    CCScene *scene = [CCScene node];
//	
//	// 'layer' is an autorelease object.
//	GameLevelLayer *layer = [GameLevelLayer node];
//	
//	// add layer as a child to scene
//	[scene addChild: layer];
//	
//	// return the scene
//	return scene;
//}
//-(void)levelChange:(CCMenuItem *) menuItem
//{
//    int param = menuItem .tag;
//    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer showLevel:param score:0 concrete:0]]];
//    
//}
//-(void)menuChange
//{
//    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[MenuLayer scene]]];
//}
////-(void)random
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:0]]];
////}
////-(void)random2
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:1]]];
////}
////-(void)random3
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:2]]];
////}
////-(void)random4
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:3]]];
////}
////-(void)random5
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:4]]];
////}
////-(void)random6
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:5]]];
////}
////-(void)random7
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:6]]];
////}
////-(void)random8
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:7]]];
////}
////-(void)random9
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:8]]];
////}
////-(void)random10
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:9]]];
////}
////-(void)random11
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:10]]];
////}
////-(void)random12
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:11]]];
////}
////-(void)random13
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:12]]];
////}
////-(void)random14
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:13]]];
////}
////-(void)random15
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:14]]];
////}
////-(void)random16
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:15]]];
////}
////-(void)random17
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:16]]];
////}
////-(void)random18
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:17]]];
////}
////-(void)random19
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:18]]];
////}
////-(void)random20
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:19]]];
////}
////-(void)random21
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:20]]];
////}
////-(void)random22
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:21]]];
////}
////-(void)random23
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:22]]];
////}
////-(void)random24
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:23]]];
////}
////-(void)random25
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:24]]];
////}
////-(void)random26
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:25]]];
////}
////-(void)random27
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:26]]];
////}
////-(void)random28
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:27]]];
////}
////-(void)random29
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:28]]];
////}
////-(void)random30
////{
////    [[CCDirector sharedDirector]replaceScene:[CCTransitionShrinkGrow transitionWithDuration:0.6f scene:[GameLayer showLevel:29]]];
////}
//
//@end
