//
//  SquarePoints.h
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "Box2D.h"

@interface SquarePoints : CCSprite
{
    bool prev;
    int previous;
    bool start;
    bool mountain;
    bool teleporter;
    bool bridges;
    
    CGPoint pair;
    CGPoint pairA;
    CGPoint pair2;
    CGPoint pair2A;
    CGPoint pair3;
    
}
@property bool selected;
@property bool built;

+(id)square;
-(id)initWithSquareImage;
-(void) setTrue;
-(void) setFalse;
-(bool) getState;
-(void) setPoint:(int) k;
-(int) returnPoint;
-(void) setStart;
-(bool) returnStart;
-(void) setMountain;
-(bool) returnMountain;
-(void) setPair:(CGPoint) i second:(CGPoint) j;
-(CGPoint) returnPair;
-(CGPoint) returnPairA;
-(void) setTeleporter;
-(bool) returnTeleporter;
-(void) setPair2:(CGPoint) i second:(CGPoint) j;
-(CGPoint) returnPair2;
-(CGPoint) returnPair2A;
-(void)setOtherMountain;
-(bool)returnOtherMountain;
-(void)setPair3:(CGPoint)i;
-(CGPoint)returnPair3;
@end
