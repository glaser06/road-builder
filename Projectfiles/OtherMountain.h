//
//  OtherMountain.h
//  squirrellls
//
//  Created by Zaizen Kaegyoshi on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "Box2D.h"
#import "GameLayer.h"

@interface OtherMountain : CCSprite
{
    
}
+(id)OtherMountain;
-(id)initWithOtherMountainImage;

@end
