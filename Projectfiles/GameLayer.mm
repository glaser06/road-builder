/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim.
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "GameLayer.h"
#import "MenuLayer.h"
#import "SimpleAudioEngine.h"
#import "SquarePoints.h"
#import "Mountains.h"
#import "GamePauseLayer.h"
#import "OtherMountain.h"
#import "GameLossLayer.h"

#define d(p1x, p1y, p2x, p2y) sqrt((p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y))
int countscenes = 0;
bool bigMountains;
int currentScene;
int roads = 0;
int totalconcrete = 0;
@implementation GameLayer

//Create the bullets, add them to the list of bullets so they can be referred to later


-(id) init
{
	if ((self = [super init]))
	{
        //UIFont *customFont = [UIFont fontWithName:@"frutiger-lt-std-75.otf" size:25];
        time = 0 ;
        count2 = 0;
        NSNumber *highScore2 = [NSNumber numberWithInteger:countscenes];
        [[NSUserDefaults standardUserDefaults] setObject:highScore2 forKey:@"levels"];
        firstPoint = NULL;
        selected = false;
        start = true;
        solved = false;
        tutorial = [[[NSUserDefaults standardUserDefaults]objectForKey:@"tutorial"]boolValue];
        
        if(countscenes==1)
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"LLSDemo-Just-Us-Cats.mp3"];
        else
            [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
        
        
        CCSprite *background = [CCSprite spriteWithFile:@"construction_app_gameplay1.png"];
        background.position = CGPointZero;
        background.anchorPoint=CGPointZero;
        [self addChild:background z:-1];
        
        ccDrawColor4F(.5f, .5f, .6f, .4f);
        ccDrawLine(ccp(430,0), ccp(430,320));
        
        drawPt = [NSMutableArray array];
        drawPts = [NSMutableArray array];
        selectedPts = [NSMutableArray array];
        score=9001;
        count=0;
        
        CCMenuItemImage * menuItem1 = [CCMenuItemImage itemWithNormalImage:@"pause_btn.png"
                                                             selectedImage: @"pause_btn.png"
                                                                    target:self
                                                                  selector:@selector(random1)];
        CCMenu *myMenu = [CCMenu menuWithItems:menuItem1, nil];
        myMenu.position = CGPointMake([[CCDirector sharedDirector]screenSize].width-30, 30);
        [self addChild:myMenu];
        
        CCSprite *levelBG = [CCSprite spriteWithFile:@"level_btn.png"];
        levelBG.position=ccp([[CCDirector sharedDirector]screenSize].width-30,295);
        [self addChild:levelBG];
        
        label2 = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", roads]fontName:@"Frutiger LT Std" fontSize:25];
        label2.position = CGPointMake([[CCDirector sharedDirector]screenSize].width-30, 290);
        glLineWidth(2.0f);
        label2.color = ccGRAY;
        [self addChild:label2 z:100];
        
//        roadLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i",roads] fontName:@"Frutiger LT Std" fontSize:25];
//        roadLabel.position = CGPointMake([[CCDirector sharedDirector]screenSize].width-30, 240);
//        roadLabel.color = ccGRAY;
//        [self addChild:roadLabel z:200];
//        
//        CCSprite *roadBG = [CCSprite spriteWithFile:@"level_btn.png"];
//        roadBG.position=ccp([[CCDirector sharedDirector]screenSize].width-30,245);
//        [self addChild:roadBG];
        
        
        
        instruction1 = [CCLabelTTF labelWithString:@"" fontName:@"Frutiger LT Std" fontSize:20];
        score=0;
        //        label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", 0]
        //                                               fontName:@"Frutiger LT Std"
        //                                               fontSize:20];
        //        label.position = CGPointMake(440, 300);
        //        glLineWidth(2.0f);
        //        label.color = ccRED;
        //        [self addChild:label];
        //        label1 = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", 0]
        //                                   fontName:@"Frutiger LT Std"
        //                                   fontSize:20];
        //        label1.position = CGPointMake(20, 300);
        //        glLineWidth(2.0f);
        //        label1.color = ccRED;
        //        [self addChild:label1];
        
        
        
//        ppos= [NSMutableArray array];
//        ppos1 = [NSMutableArray array];
//        ppos2 = [NSMutableArray array];
        counting = [NSMutableArray array];
        counting1 = [NSMutableArray array];
        
        
//        NSMutableArray *levels = [NSMutableArray array];
//        NSMutableArray *mountlevels = [NSMutableArray array];
//
//        
//        
//        
//        
//        NSDictionary *level = [NSDictionary dictionary];
        
        storage1=[NSMutableArray array];
        storage2=[NSMutableArray array];
        
        
        
        
        
        
        
        //blocks2 = [level objectForKey:@"blocks"];
        if(!tutorial)
        {
            blocks2 = [self generateBlocks];
            mountain = [self generateMountains];
            teleporter1 = [self generateTeleports];
        }
        else
        {
            [self tutoriallvl];
        }
        limit = [self calculateShortest];
        label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", limit]
                                   fontName:@"Frutiger LT Std"
                                   fontSize:20];
        label.position = CGPointMake([[CCDirector sharedDirector]screenSize].width-35, 160);
        glLineWidth(2.0f);
        label.color = ccBLACK;
        [self addChild:label];
        
        //        myPlist = [something getPlist];
        //        NSArray *myArray = [myPlist objectFOrKey:@"array"];
        //        for(int i = 0; i<[myArray count]; i++)
        //        {
        //            Block *newBlock = [Block block];
        //            newBlock.position = [myArray objectAtIndex:i);
        //            [self addChild:newBlock z:1 tag:i];
        //        }
        //
        //
        //        Block *newBlock = [Block block];
        //        newBlock.position = ccp(100,20);
        //        [self addChild:newBlock z:1 tag:1];
        
        //[self getChildByTag:1];
        for(int i  = 100;i<(int)mountain.count+100;i++)
        {
            Mountains *mountain3 = [Mountains mountain];
            //mountain3.position = CGPointMake([[[mountain objectAtIndex:i-100]objectForKey:@"x"]floatValue],[[[mountain objectAtIndex:i-100]objectForKey:@"y"]floatValue]);
            mountain3.position = [[mountain objectAtIndex:i-100]CGPointValue];
            [self addChild:mountain3 z:7 tag:i];
        }
        for(int i = 51; i < (int)[teleporter1 count]+51; i++)
        {
            Teleporter *teleport2 = [Teleporter teleport];
            teleport2.position=[[teleporter1 objectAtIndex:i-51]CGPointValue];
            //teleport2.position = CGPointMake([[[teleporter1 objectAtIndex:i-51]objectForKey:@"x"]floatValue],[[[teleporter1 objectAtIndex:i-51]objectForKey:@"y"]floatValue]);
            [self addChild:teleport2 z:7 tag:i];
        }
        
//        for(int i = 150;i<(int)mountain2.count +150;i++)
//        {
//            OtherMountain *othermount = [OtherMountain OtherMountain];
//            othermount.position = CGPointMake([[[mountain2 objectAtIndex:i-150]objectForKey:@"x"]floatValue],[[[mountain2 objectAtIndex:i-150]objectForKey:@"y"]floatValue]);
//            [self addChild:othermount z:7 tag:i];
//        }
        
        
        for(int i=0;i<(int)blocks2.count;i++)
        {
            //NSDictionary *bl = [blocks2 objectAtIndex:i];
            SquarePoints *squares = [SquarePoints square];
            //sprite = [CCSprite spriteWithFile:[bl objectForKey:@"spritename"]];
            squares.position = [[blocks2 objectAtIndex:i]CGPointValue];
            
            //squares.position = CGPointMake([[bl objectForKey:@"x"]floatValue],  [[bl objectForKey:@"y"]floatValue]);
            
            [self addChild:squares z:7 tag:i];
        }
        for(int i = 100;i<(int)mountain.count +100;i++)
            [self addMountain:i];
        for(int i = 51; i < (int)teleporter1.count+51; i++)
            [self addTeleporter:i];
        //        for(int i = 150;i<mountain2.count +150;i++)
        //            [self addOtherMountain:i];
        
        //        CCMenuItemImage * menuItem2 = [CCMenuItemImage itemWithNormalImage:@"myfirstbutton.png"
        //                                                             selectedImage: @"myfirstbutton_selected.png"
        //                                                                    target:self
        //                                                                  selector:@selector(random)];
        //        CCMenu *menu = [CCMenu menuWithItems:menuItem2, nil];
        //        //[menu alignItemsVertically];
        //        menu.position=CGPointMake(440, 150);
        //        [self addChild:menu];
        
        
        
		[self scheduleUpdate];
	}
    
	return self;
}

-(void)tutoriallvl
{
    int center = [[CCDirector sharedDirector]screenCenter].x;
    if(countscenes ==1)
    {
        NSMutableArray *randomPts = [NSMutableArray array];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(200,180)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(400,180)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(300,130)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(330,210)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(100,230)]];
        
        blocks2 = randomPts;
        mountain = @[];
        teleporter1 = @[];
        [instruction1 setString:@"Click one city and click another city to build a road"];
        instruction1.position = ccp(center,70);
        [self addChild:instruction1];
    }
    else if(countscenes==2)
    {
        NSMutableArray *randomPts = [NSMutableArray array];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(200,180)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(400,180)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(300,100)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(300,250)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(100,230)]];

        blocks2 = randomPts;
        NSValue *temp = [NSValue valueWithCGPoint:ccp(350,215)];
        mountain = @[temp];
        
        teleporter1 = @[];
        [instruction1 setString:@"Gravel roads use twice as much \n concrete as normal roads, so avoid it"];
        instruction1.position = ccp(center,50);
        [self addChild:instruction1];
    }
    else if(countscenes == 3)
    {
        NSMutableArray *randomPts = [NSMutableArray array];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(200,180)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(400,180)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(300,100)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(300,220)]];
        [randomPts addObject:[NSNumber valueWithCGPoint:ccp(390,50)]];
        blocks2 = randomPts;
        mountain = @[];
        teleporter1 = [self generateTeleports];
        [instruction1 setString:@"Dirt roads use half as much \n concrete as normal roads"];
        instruction1.position = ccp(center,70);
        [self addChild:instruction1];
        [[NSUserDefaults standardUserDefaults] setObject:@false forKey:@"tutorial"];

    }
    else
    {
        roads = 0;
        [instruction1 setString:@"Build!"];
        instruction1.position = ccp(284,300);
        [self addChild:instruction1];
        blocks2 = [self generateBlocks];
        mountain = [self generateMountains];
        teleporter1 = [self generateTeleports];
    }
}


-(void) update:(ccTime)delta
{
    
    
    
    //Check fori inputs and create a bullet if there is a tap
    KKInput* input = [KKInput sharedInput];
    
    
    if(input.anyTouchBeganThisFrame)
    {
        touch = [input anyTouchLocation];
        [self checkTap];
        if(tutorial && [storage1 count]==1 && countscenes ==1)
        {
            [instruction1 setString:@("Connect all the cities with roads \n to finish the level")];
        }
        else if(tutorial && [storage1 count]==2 && countscenes ==1)
        {
            [instruction1 setString:@("use the least amount of concrete \n to build the most amount of roads")];
            CCLabelTTF *instruction2 = [CCLabelTTF labelWithString:@"concrete" fontName:@"Frutiger LT Std" fontSize:20];
            instruction2.position = ccp([[CCDirector sharedDirector]screenSize].width-50,140);
            [self addChild:instruction2];
            CCLabelTTF *instruction3 = [CCLabelTTF labelWithString:@"roads" fontName:@"Frutiger LT Std" fontSize:20];
            instruction3.position = ccp([[CCDirector sharedDirector]screenSize].width-40,260);
            [self addChild:instruction3];
        }
        else if(tutorial && [storage1 count]==3 && countscenes == 1)
        {
            
        }
        
      
    }
    
    if(solved)
    {
        if(limit<-2)
        {
            if(limit<0)
                [label setString:[NSString stringWithFormat:@"%i",0]];
            else [label setString:[NSString stringWithFormat:@"%i",0]];
            [self pauseSchedulerAndActions];
            [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
            [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLossLayer scene]]];
            if(tutorial)
            {
                [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer showLevel:countscenes score:roads concrete:totalconcrete]]];
            }
            return;
        }
        else
        {
            [label setString:[NSString stringWithFormat:@"%i",limit]];
            
            [self gameWinChange];
        }
    }
    count2++;
    if(count2>58)
    {
        time++;
        count2 = 0;
        
    }
    //    [label1 setString:[NSString stringWithFormat:@"%i",time]];
    
    
    
    
    
    
}
-(void)gameWinChange
{
    [self pauseSchedulerAndActions];
    [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    
    //int param = menuItem .tag;
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer showLevel:countscenes+1 score:roads concrete:totalconcrete]]];
    
    //[[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameOverLayer scene]]];
}
-(void)checkTap
{
    int index = [self checkSquare];
    if(index>-1)
    {
        SquarePoints *temp = (SquarePoints*)[self getChildByTag:index];
        
        
        if(selected==true)
        {
            if(temp.selected==true)
            {
                temp.selected=false;
                selected=false;
                
                [selectedPts removeObject:temp];
            }
            else
            {
                if(start)
                {
                    firstPoint = square;
                    start = false;
                }
                else
                {
                    square = square2;
                    
                }
                for(int i=0;i<(int)[selectedPts count];i++)
                {
                    SquarePoints *x = [selectedPts objectAtIndex:i];
                    x.selected=false;
                }
                [selectedPts removeAllObjects];
                selected = true;
                
                if(![temp built])
                {
                    square2 = temp;
                    int cost = [self cost];
                    limit-=cost;
                    totalconcrete+=cost;
                    roads++;
                    [[SimpleAudioEngine sharedEngine] playEffect:@"explo2.wav"];

                    if(limit<-2)
                    {
//                        limit = 0;
                        [label setString:[NSString stringWithFormat:@"%i",limit]];
                        [self pauseSchedulerAndActions];
                        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
                        [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLossLayer scene]]];
                        if(tutorial)
                        {
                            [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer showLevel:countscenes score:roads concrete:totalconcrete]]];
                            
                        }
                        return;
                    }
                    
                    difficulty+=.3;
                    [label2 setString:[NSString stringWithFormat:@"%i",roads]];
                    [drawPt addObject:[NSNumber valueWithCGPoint:square.position]];
                    [drawPts addObject:[NSNumber valueWithCGPoint:square2.position]];
                    [storage1 addObject:square];
                    square2.built = true;
                    square.built=true;
                    [label setString:[NSString stringWithFormat:@"%i",limit]];
                                        
                }
                else if([self checkAllBuilt])
                {
                    if(CGPointEqualToPoint(temp.position, firstPoint.position))
                    {
                        
                        
                        square2 = temp;
                        int cost = [self cost];
                        limit-=cost;
                        totalconcrete+=cost;
                        roads++;
                        [[SimpleAudioEngine sharedEngine] playEffect:@"explo2.wav"];

                        if(limit<-2)
                        {
//                            limit = 0;
                            [label setString:[NSString stringWithFormat:@"%i",limit]];
                            [self pauseSchedulerAndActions];
                            [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
                            [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLossLayer scene]]];
                            if(tutorial)
                            {
                                [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer showLevel:countscenes score:roads concrete:totalconcrete]]];
                            }
                            return;
                        }
                        
                        [label2 setString:[NSString stringWithFormat:@"%i",roads]];
                        [drawPt addObject:[NSNumber valueWithCGPoint:square.position]];
                        [drawPts addObject:[NSNumber valueWithCGPoint:square2.position]];
                        [storage1 addObject:square];
                        square2.built = true;
                        square.built=true;
                        solved=true;
                    }
                }
                
            }
        }
        else
        {
            if(temp.selected==true)
            {
                temp.selected=false;
                selected=false;
                
                [selectedPts removeObject:temp];
            }
            else
            {
                if(start)
                {
                    square = temp;
                    [selectedPts addObject:temp];
                    temp.selected = true;
                    selected = true;
                    
                }
            }
        }
        
    }

}
-(bool)checkAllBuilt
{
    for(int i=0;i<(int)[blocks2 count];i++)
    {
        SquarePoints *temp = (SquarePoints*)[self getChildByTag:i];
        if(![temp built])
        {
            return false;
        }
    }
    return true;
}

-(int)cost
{
    CGPoint mount1 = [square returnPair];
    //CGPoint mount2 = [square2 returnPair];
    CGPoint tele1 = [square returnPair2];
    //CGPoint tele2 = [square2 returnPair2];
    if(([square returnMountain]==true))
    {
        if(CGPointEqualToPoint(mount1, square2.position))
        {
            return d(square.position.x, square.position.y, square2.position.x, square2.position.y)*3;
        }
    }
    if([square returnTeleporter]==true)
    {
        if(CGPointEqualToPoint(tele1, square2.position))
        {
            return d(square.position.x, square.position.y, square2.position.x, square2.position.y)/2;
        }
    }
    return d(square.position.x, square.position.y, square2.position.x, square2.position.y);
}

-(void)random
{
    [[CCDirector sharedDirector]replaceScene:[MenuLayer scene]];
}

-(void) random1
{
    [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    [[CCDirector sharedDirector]pushScene:[CCTransitionFade transitionWithDuration:.6f scene:[GamePauseLayer scene]]];
}
-(void) random2
{
    [self pauseSchedulerAndActions];
    [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLossLayer scene]]];
}

-(void)draw
{
    //ptvtrue=false;
    //    int superb=0;
    ccDrawColor4F(1.0f, 1.0f, 1.0f, 1.0f);
    for(int i=0;i<(int)[selectedPts count];i++)
    {
        SquarePoints *temp = [selectedPts objectAtIndex:i];
        ccDrawColor4B(255, 255, 199,2000);
        ccDrawCircle(temp.position, 20, 0, 50, NO);
    }
    if(!(CGPointEqualToPoint(CGPointZero, square2.position)))
        ccDrawCircle(square2.position, 20, 0, 50, NO);
    ccDrawColor4F(1.0f, 0, 0, 0);
    if(!(CGPointEqualToPoint(CGPointZero, firstPoint.position)))
        ccDrawCircle(firstPoint.position, 20, 0, 50, NO);
    
    
    //
    //    for(int i =0;i<(int)[drawPt count];i++)
    //    {
    //        NSValue *v = [drawPt objectAtIndex:i];
    //        NSValue *v1 = [drawPts objectAtIndex:i];
    //        CGPoint cgp = [v CGPointValue];
    //        CGPoint cgp2 = [v1 CGPointValue];
    //        //glColor4ub(25,100,255,255);
    //        ccDrawColor4F(.4f, .4f, .4f, .4f);
    //        ccDrawLine(cgp, cgp2);
    //    }
    
    //CCSpriteBatchNode *batch = [CCSpriteBatchNode batchNodeWithFile:@"concreteroad_horz.png"];
    
    
    for(int i=0;i<(int)[drawPt count];i++)
    {
        CGPoint p0 = [[drawPt objectAtIndex:i]CGPointValue];
        CGPoint p1 = [[drawPts objectAtIndex:i]CGPointValue];
        CCSprite *sprites = [CCSprite spriteWithFile:@"concreteroad_horz.png" rect:CGRectMake(0, 0, d(p0.x, p0.y, p1.x, p1.y) , 30)];
        //CCSprite *sprite = [CCSprite spriteWithBatchNode:batch rect:CGRectMake(0, 0, d(p0.x, p0.y, p1.x, p1.y) , 30)];
        sprites.position = ccp((p0.x + p1.x)/2, (p0.y + p1.y)/2);
        sprites.rotation = CC_RADIANS_TO_DEGREES(atan((p0.y - p1.y)/(p0.x - p1.x))) * -1;
        //[batch addChild:sprites];
        [self addChild:sprites ];
        
        
    }
    [drawPt removeAllObjects];
    [drawPts removeAllObjects];
    

}

-(double)highScore:(int)j
{
    NSNumber *currentHighScore = [[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreM"];
    int hs = [currentHighScore intValue];
    
    
    SquarePoints *temp,*temp1;
    temp = (SquarePoints *)[self getChildByTag:j];
    int i = [temp returnPoint];
    CGPoint temps = [temp returnPair];
    // CGPoint tempsA = [temp returnPairA];
    CGPoint temps2 = [temp returnPair2];
    CGPoint temps3 = [temp returnPair3];
    // CGPoint temps2A = [temp returnPair2A];
    temp1 = (SquarePoints *)[self getChildByTag:i];
    double highscore;
    if(([temp returnMountain]==true)||([temp returnTeleporter]==true)||([temp returnOtherMountain]==true))
    {
        if(CGPointEqualToPoint(temp1.position, temps))
        {
            //            if(bigMountains==false)
            //            {
            //                [self random3];
            //            }
            
            highscore = (sqrt((temp.position.x - temp1.position.x)*(temp.position.x - temp1.position.x)+(temp.position.y-temp1.position.y)*(temp.position.y-temp1.position.y)))*3 ;
            //highscore = hs/5;
            
        }
        else if(CGPointEqualToPoint(temp1.position, temps3))
        {
            if(bigMountains==false)
            {
                [self random3];
            }
            highscore = hs/4;
            
        }
        else if(CGPointEqualToPoint(temp1.position, temps2))
        {
            if(bigMountains == false)
                highscore = (sqrt((temp.position.x - temp1.position.x)*(temp.position.x - temp1.position.x)+(temp.position.y-temp1.position.y)*(temp.position.y-temp1.position.y)))/2 ;
            else
            {
                highscore = ((sqrt((temp.position.x - temp1.position.x)*(temp.position.x - temp1.position.x)+(temp.position.y-temp1.position.y)*(temp.position.y-temp1.position.y)))/2) ;
            }
        }
        else
        {
            if(bigMountains == false)
                highscore = (sqrt((temp.position.x - temp1.position.x)*(temp.position.x - temp1.position.x)+(temp.position.y-temp1.position.y)*(temp.position.y-temp1.position.y))) ;
            else
                highscore = (sqrt((temp.position.x - temp1.position.x)*(temp.position.x - temp1.position.x)+(temp.position.y-temp1.position.y)*(temp.position.y-temp1.position.y))) ;
        }
    }
    //    else
    //        highscore = (sqrt((temp.position.x - temp1.position.x)*(temp.position.x - temp1.position.x)+(temp.position.y-temp1.position.y)*(temp.position.y-temp1.position.y))) ;
    //    else if([temp returnTeleporter]==true)
    //    {
    //        if((CGPointEqualToPoint(temp1.position, temps2))||(CGPointEqualToPoint(temp1.position, temps2A)))
    //        {
    //            highscore = (sqrt((temp.position.x - temp1.position.x)*(temp.position.x - temp1.position.x)+(temp.position.y-temp1.position.y)*(temp.position.y-temp1.position.y)))/2 ;
    //        }
    //        else
    //            highscore = (sqrt((temp.position.x - temp1.position.x)*(temp.position.x - temp1.position.x)+(temp.position.y-temp1.position.y)*(temp.position.y-temp1.position.y))) ;
    //    }
    else
        highscore = (sqrt((temp.position.x - temp1.position.x)*(temp.position.x - temp1.position.x)+(temp.position.y-temp1.position.y)*(temp.position.y-temp1.position.y))) ;
    return highscore;
}
-(int)calculateShortest
{
    int shortest = 90000;
    for(int i=0;i<(int)[blocks2 count];i++)
    {
//        CGPoint x =  [[blocks2 objectAtIndex:i]CGPointValue];
        int dist = [self calculateLimit:i];
//        NSDate *future = [NSDate dateWithTimeIntervalSinceNow: 1 ];
//        [NSThread sleepUntilDate:future];
        if(dist<shortest)
        {
            
            shortest = dist;
        }
        
    }
    
    return [self calculateLimit:0];
}
-(int)calculateLimit:(int) j
{
    NSMutableArray *points = [NSMutableArray array];
    //NSMutableArray *dirtRds = [NSMutableArray array];
    
    for(int i=0;i<(int)[blocks2 count];i++)
    {
        [points addObject:[blocks2 objectAtIndex:i]];
    }
    CGPoint startPt = [[blocks2 objectAtIndex:j]CGPointValue];
    int distance = 900000;
    int total = 0;
    int shortestInd;
    CGPoint point  = CGPointZero;
    CGPoint point2 = startPt;
    [points removeObjectAtIndex:j];
    bool complete=false;
    while(!complete)
    {
        
        distance = 900000;
        for(int i=0;i<(int)[points count];i++)
        {
            point = [[points objectAtIndex:i]CGPointValue];
            int temp = d(point2.x,point2.y,point.x,point.y);
            for(int i=0;i<(int)[teleporter1 count];i++)
            {
                CGPoint midpt1 = [[teleporter1 objectAtIndex:i]CGPointValue];
                CGPoint midpt2 = ccp((int)((point.x+point2.x)/2),(int)((point.y+point2.y)/2));
                if(CGPointEqualToPoint(midpt1, midpt2))
                {
                    temp = temp/2;
                }
            }
            for(int i=0;i<(int)[mountain count];i++)
            {
                CGPoint midpt1 = [[mountain objectAtIndex:i]CGPointValue];
                CGPoint midpt2 = ccp((int)((point.x+point2.x)/2),(int)((point.y+point2.y)/2));
                if(CGPointEqualToPoint(midpt1, midpt2))
                {
                    temp = temp*3;
                }
            }
            if(temp<distance)
            {
                shortestInd = i;
                distance = temp;
            }
        }
        point2 = [[points objectAtIndex:shortestInd]CGPointValue];
        total+=distance;
        [points removeObjectAtIndex:shortestInd];
        if([points count] == 0)
        {
            int temp  = d(point2.x,point2.y,startPt.x,startPt.y);
            total+=temp;
            complete=true;
        }
        
    }
    return total;
    //    for(int i=0;i<[blocks2 count];i++)
    //    {
    //        if(i!=3)
    //        {
    //            CGPoint point = [[blocks2 objectAtIndex:i]CGPointValue];
    //            int distance = d(startingPt.x,startingPt.y,point.x,point.y);
    //
    //        }
    //    }
}
-(void)random3
{
    [[CCDirector sharedDirector]pushScene:[CCTransitionFade transitionWithDuration:0.5f scene:[GameLayer bigMountain:countscenes-20]]];
}
-(bool)setStart:(int) j
{
    int counter =0;
    for(int i=0;i<(int)[blocks2 count];i++)
    {
        square = (SquarePoints *)[self getChildByTag:i];
        
        if([square returnStart]!=true)
        {
            counter++;
            //else return false;
        }
    }
    if(counter == (int)[blocks2 count])
    {
        square = (SquarePoints *)[self getChildByTag:j];
        [square setStart];
        firstPoint = (SquarePoints *)[self getChildByTag:j];
        return true;
    }
    return false;
}
-(bool)checkStart
{
    int counter = 0;
    for(int i=0;i<(int)[blocks2 count];i++)
    {
        SquarePoints *square3 = (SquarePoints *)[self getChildByTag:i];
        if([square3 getState]!=true)
        {
            if([square3 returnStart] !=true)
            {
                counter++;
            }
        }
        
    }
    if(counter>0)
        return false;
    else
        return true;
}

+(id) bigMountain: (int) i
{
    currentScene= i;
    
    bigMountains = true;
    CCScene* myScene = [GameLayer scene];
    return myScene;
}
+(id) showLevel: (int) i score: (int) s concrete:(int)c
{
    countscenes = i;
    roads = s;
    totalconcrete = c;
    CCScene* myScene = [GameLayer scene];
    return myScene;
}
-(void)restartGame
{
    
}
+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameLayer *layer = [GameLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(NSMutableArray*)generateBlocks
{
    NSMutableArray *randomPts = [NSMutableArray array];
    for(int i=1;i<=countscenes+5;i++)
    {
        int width = [[CCDirector sharedDirector]screenSize].width;
        CGPoint randomPt = ccp(arc4random()%(width-80)+20,arc4random()%280+20);
        CGPoint prev;
        CGRect rand1 = CGRectMake(prev.x, prev.y, 40, 40);
        CGRect rand = CGRectMake(randomPt.x, randomPt.y, 40, 40);
        
        while(true)
        {
            int flag = 0;
            for(int j=0;j<(int)[randomPts count];j++)
            {
                prev = [[randomPts objectAtIndex:j]CGPointValue];
                rand1 = CGRectMake(prev.x, prev.y, 40, 40);
                if(CGRectIntersectsRect(rand,rand1))
                {
                    flag=1;
                }
                
            }
            if(flag==0)
            {
                break;
            }
            else
            {
                randomPt = ccp(arc4random()%470+20,arc4random()%260+20);
                rand = CGRectMake(randomPt.x, randomPt.y, 40, 40);
            }
            
        }
        
        
        
        [randomPts addObject:[NSValue valueWithCGPoint:randomPt]];
    }
    
    return randomPts;
}
-(NSMutableArray*)generateMountains
{
    NSMutableArray *randomMts = [NSMutableArray array];
    //int num = arc4random()%(countscenes+1);
    
    for(int i=1;i<=(int)[blocks2 count];i++)
    {
//        int rand = arc4random()%12;
        if(i%(10-countscenes)==0)
        {
            CGPoint bl1Pt = [[blocks2 objectAtIndex:i-1]CGPointValue];
            CGPoint bl2Pt = [[blocks2 objectAtIndex:i-2]CGPointValue];
            CGPoint midPt = ccp(((bl1Pt.x+bl2Pt.x)/2),((bl1Pt.y+bl2Pt.y)/2));
            
            
            int flag = 0;
            for(int i=0;i<(int)[blocks2 count];i++)
            {
                CGPoint blockPt = [[blocks2 objectAtIndex:i]CGPointValue];
                CGRect rand1 = CGRectMake(midPt.x, midPt.y, 40, 40);
                CGRect rand = CGRectMake(blockPt.x, blockPt.y, 40, 40);
                if(CGRectIntersectsRect(rand1, rand))
                {
                    flag=1;
                }
                
            }
            if(flag==0)
                [randomMts addObject:[NSValue valueWithCGPoint:midPt]];
            
            
            
        }
        
        
        
        
    }
    return randomMts;
}
-(NSMutableArray*)generateTeleports
{
    NSMutableArray *randomRds = [NSMutableArray array];
    //int num = arc4random()%(countscenes+1);
    for(int i=1;i<=(int)[blocks2 count];i++)
    {
//        int rand = arc4random()%12;
        if(i%(11-countscenes)==0)
        {
            int flag = 0;
            CGPoint bl1Pt = [[blocks2 objectAtIndex:i-1 ]CGPointValue];
            CGPoint bl2Pt = [[blocks2 objectAtIndex:i-2]CGPointValue];
            CGPoint midPt = ccp(((bl1Pt.x+bl2Pt.x)/2),((bl1Pt.y+bl2Pt.y)/2));
            for(int i=0;i<(int)[mountain count];i++)
            {
                CGPoint mtPt = [[mountain objectAtIndex:i]CGPointValue];
                if(CGPointEqualToPoint(midPt, mtPt))
                {
                    flag =1;
                }
            }
            if(flag ==0)
                [randomRds addObject:[NSValue valueWithCGPoint:midPt]];
        }
    }
    return randomRds;
}

-(void)addMountain:(int)c
{
    
    for(int i = 0;i<(int)blocks2.count;i++)
    {
        for(int j = 0; j<(int)blocks2.count; j++)
        {
            if(i!=j)
            {
                
                float slope2;
                SquarePoints *x,*y;
                Mountains *z;
                x = (SquarePoints *)[self getChildByTag:i];
                y = (SquarePoints *)[self getChildByTag:j];
                float slope = (y.position.y-x.position.y)/(y.position.x-x.position.x);
                z = (Mountains *)[self getChildByTag:c];
                
                slope2 = ((z.position.y)-x.position.y)/((z.position.x)-x.position.x);
                
                
                if(slope == slope2)
                {
                    [x setMountain];
                    [y setMountain];
                    [x setPair:y.position second:x.position];
                    [y setPair:x.position second:y.position];
                    
                    CGPoint p0 = x.position;
                    CGPoint p1 = y.position;
                    CCSprite *sprite = [CCSprite spriteWithFile:@"gravelroad_horz.png" rect:CGRectMake(0, 0, d(p0.x, p0.y, p1.x, p1.y) , 30)];
                    
                    sprite.position = ccp((p0.x + p1.x)/2, (p0.y + p1.y)/2);
                    sprite.rotation = CC_RADIANS_TO_DEGREES(atan((p0.y - p1.y)/(p0.x - p1.x))) * -1;
                    [self addChild:sprite];
                    
                }
                //                bool idk = [x returnMountain];
                //                idk = [y returnMountain];
                
            }
        }
    }
}
-(void)addTeleporter:(int) c
{
    for(int i = 0;i<(int)blocks2.count;i++)
    {
        for(int j = 0; j<(int)blocks2.count; j++)
        {
            if(i!=j)
            {
                SquarePoints *x,*y;
                Teleporter *z;
                x = (SquarePoints *)[self getChildByTag:i];
                y = (SquarePoints *)[self getChildByTag:j];
                float slope = (y.position.y-x.position.y)/(y.position.x-x.position.x);
                z = (Teleporter *)[self getChildByTag:c];
                float slope2 = (z.position.y-x.position.y)/(z.position.x-x.position.x);
                if(slope == slope2)
                {
                    if(([x returnTeleporter]!=true) && ([y returnTeleporter]!=true))
                    {
                        [x setTeleporter];
                        [y setTeleporter];
                        [x setPair2:y.position second:x.position];
                        [y setPair2:x.position second:y.position];
                        CGPoint p0 = x.position;
                        CGPoint p1 = y.position;
                        CCSprite *sprite = [CCSprite spriteWithFile:@"dirtroad_horz.png" rect:CGRectMake(0, 0, d(p0.x, p0.y, p1.x, p1.y) , 30)];
                        
                        sprite.position = ccp((p0.x + p1.x)/2, (p0.y + p1.y)/2);
                        sprite.rotation = CC_RADIANS_TO_DEGREES(atan((p0.y - p1.y)/(p0.x - p1.x))) * -1;
                        [self addChild:sprite];
                        //                        [x setPair2:y.position:];
                        //                        [y setPair2:x.position];
                        
                    }
                }
                //                bool idk = [x returnMountain];
                //                idk = [y returnMountain];
            }
        }
    }
}
-(void)addOtherMountain:(int) c
{
    for(int i = 0;i<(int)blocks2.count;i++)
    {
        for(int j = 0; j<(int)blocks2.count; j++)
        {
            if(i!=j)
            {
                SquarePoints *x,*y;
                OtherMountain *z;
                x = (SquarePoints *)[self getChildByTag:i];
                y = (SquarePoints *)[self getChildByTag:j];
                float slope = (y.position.y-x.position.y)/(y.position.x-x.position.x);
                z = (OtherMountain *)[self getChildByTag:c];
                float slope2 = (z.position.y-x.position.y)/(z.position.x-x.position.x);
                if(slope == slope2)
                {
                    if(([x returnOtherMountain]!=true) && ([y returnOtherMountain]!=true))
                    {
                        [x setOtherMountain];
                        [y setOtherMountain];
                        [x setPair3:y.position];
                        [y setPair3:x.position];
                        //                        [x setPair2:y.position:];
                        //                        [y setPair2:x.position];
                        
                    }
                }
                //                bool idk = [x returnMountain];
                //                idk = [y returnMountain];
            }
        }
    }
}

-(int)checkSquare
{
    for(int i=0;i<(int)[blocks2 count];i++)
    {
        //SquarePoints *temp = (SquarePoints*)[self getChildByTag:i];
//        NSDictionary *b1 = [blocks2 objectAtIndex:i];
//        NSNumber *x = [b1 objectForKey:@"x"];
//        NSNumber *y = [b1 objectForKey:@"y"];
        int x = [[blocks2 objectAtIndex:i]CGPointValue].x;
        int y = [[blocks2 objectAtIndex:i]CGPointValue].y;
        if((touch.x<x+25)&&(touch.x>x-25))
        {
            if((touch.y<y +25)&&(touch.y>y-25))
            {
                return i;
            }
        }
    }
    
    return -1;
}
-(bool)checkpoints: (CGPoint)cgp checkpointpoint: (CGPoint)cgp2 checkpointcounter: (int)k checkpointcounter: (int) j
{
    
    
    
    
    
    //CCSprite *square = [self getChildByTag:k];
    
    NSDictionary *bl = [blocks2 objectAtIndex:k];
    NSDictionary *bl1 = [blocks2 objectAtIndex:j];
    NSNumber *x =[bl objectForKey:@"x"];
    NSNumber *y =[bl objectForKey:@"y"];
    NSNumber *x1 = [bl1 objectForKey:@"x"];
    NSNumber *y1 = [bl1 objectForKey:@"y"];
    if((cgp.x<[x floatValue]+50)&&(cgp.x>[x floatValue]-50))
    {
        if((cgp.y<[y floatValue]+50)&&(cgp.y>[y floatValue]-50))
        {
            if((cgp2.x<[x1 floatValue]+50)&&(cgp2.x>[x1 floatValue]-50))
            {
                if((cgp2.y<[y1 floatValue]+50)&&(cgp2.y>[y1 floatValue]-50))
                {
                    //if(k==j)
                    //  return false;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;
    }
    else 
        return false;
    
}
+(int)getRoads
{
    return roads;
}
+(int)getConcrete
{
    return totalconcrete;
}
+(int)getLevel
{
    return countscenes;
}
@end
