/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "cocos2d.h"
#import "Box2D.h"
#import "SquarePoints.h"
#import "Mountains.h"
#import "Teleporter.h"

enum
{
	kTagBatchNode,
};

@interface GameLayer : CCLayer
{
    
	b2World* world;
    int currentBullet;
    
    CCSprite *projectile;
    //CCSprite *block;
    CGRect firstrect;
    CGRect secondrect;
    NSMutableArray *bullets;
    NSMutableArray *blocks;
    //int count;
    
    CCSprite *acorn;
    
    CCAction *fly;
    
    NSMutableArray *flyingFrames;
    int count;
    //int countscenes;
    int score;
    int time;
    int count2;
    int limit;
    int tempScore;

//    int roads;
    int difficulty;
    bool restart;
    bool tutorial;
    
    NSMutableArray *counting,*counting1;
    NSMutableArray *blocks2;
    NSArray *mountain;
    NSArray *teleporter1;
    NSArray *mountain2;
    int bridgeTime;
    int countBridge;
    NSMutableArray *storage1;
    NSMutableArray *storage2;
    CCLabelTTF *label;
    CCLabelTTF *label1;
    CCLabelTTF *label2;
    CCLabelTTF *roadLabel;
    CCLabelTTF *instruction1;
    SquarePoints *square;
    SquarePoints *square2;
    SquarePoints *tempSqr;
    SquarePoints *firstPoint;
    SquarePoints *prevPoint;
    //SquarePoints *x;
    NSMutableArray *drawPt;
    NSMutableArray *drawPts;
    //bool bigMountains;
    SquarePoints *selSquare;
    SquarePoints *nxtSquare;
    CGPoint touch;
    bool selected;
    NSMutableArray *selectedPts;
    bool start;
    bool solved;
    
}

//@property (nonatomic,retain) SquarePoints *square;

+(id) scene;
-(void)random;
-(void)random1;
-(void)random2;
-(bool)checkpoints: (CGPoint)cgp checkpointpoint: (CGPoint)cgp2 checkpointcounter: (int)k checkpointcounter:(int)j;

-(bool)setStart:(int)j;
-(bool)checkStart;
-(double)highScore:(int)j;
-(void)addMountain:(int)i;
-(void)addTeleporter:(int)i;
-(void)addOtherMountain:(int)i;
+(id) showLevel: (int) i score: (int) s concrete:(int)c;
+(int)getRoads;
+(int)getConcrete;
+(int)getLevel;
@end
